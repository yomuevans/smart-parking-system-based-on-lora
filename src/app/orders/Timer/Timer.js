import React, {Component} from 'react';
import P from "../../../UI/P";

class Timer extends Component {
    state = {
        sec: 0,
        min: 0,
        hour: 0
    };

    addSec = () => {
        let s = this.state.sec + 1;
        let m = this.state.min + 1;
        let h = this.state.hour + 1;
        if(s%60 === 0) {
            this.setState({sec: 0});
            if(m%60 === 0){
                this.setState({min: 0});
                this.setState({hour: h})
            } else {
                this.setState({min: m})
            }
        } else {
            this.setState({sec: s});
        }
    };

    changeToDouble = (props) => {
        if(props < 10) {
            return "0"+props;
        } else {
            return props;
        }
    };

    loop;

    componentDidMount() {
        this.loop = setInterval(this.addSec, 1000);
    }

    componentDidUpdate = () => {
        if(this.props.stop){
            clearInterval(this.loop);
        }
    };


    render() {
        return (
            <>
                <P>
                {this.state.hour > 0 && (<>{this.changeToDouble(this.state.hour)}:</>)}
                {this.changeToDouble(this.state.min)}:{this.changeToDouble(this.state.sec)}
                </P>
            </>
        )
    }
}

export default Timer;
