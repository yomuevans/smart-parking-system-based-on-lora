import React, {Component} from 'react';
import Timer from "./Timer/Timer"
import {bindActionCreators} from "redux";
import * as Actions from "../../store/actions/actions";
import {connect} from "react-redux";
import axios from "../../myaxios";
// import Checkout from "./Checkout";
import Card from "../../UI/Card";
import MainHeader from "../../UI/MainHeader";
import ListItem from "../../UI/ListItem";
import Button from "../../UI/Button";
import P from "../../UI/P";
import View from "react-native";

class StartTime extends Component {
    state = {
        reportText: "", orderId: null,
        checkout: false
    };

    // onSubmit = (e) => {
    //     e.preventDefault();
    //     axios.put("/report", {description: this.state.reportText, orderId: this.state.orderId}).then(res=>{
    //         this.props.closeDialog("changeName");
    //     })
    // };

    finishTime = () => {
        axios.put("/parkEvent").then(res => {
            if(res.data.result === "OK") {
                this.props.history.push('/user/history');
            } else {
                // this.props.showMessage({message:`Time finished!`});
                this.setState({checkout: true})
            }
        });
    };

    render() {
        const {classes} = this.props;
        return (
            <>
                <MainHeader>
                    <View>
                        <P color={"primary"}>Your park time</P>
                        <P type={"h2"}><Timer stop={this.state.checkout}/></P>

                        <Button color={"primary"}>
                            Report a problem
                        </Button>

                    </View>
                </MainHeader>

                <Card>
                    <ListItem title={"Report a problem"} button/>
                    <ListItem title={"End the time"} onPress={this.finishTime} button/>
                </Card>

                {/*<Dialog*/}
                {/*    open={this.props.dialogs['report'] ===true}*/}
                {/*    onClose={() => this.props.closeDialog("report")}*/}
                {/*    aria-labelledby="form-dialog-title"*/}
                {/*    fullWidth*/}
                {/*    className={this.state.loading ? "loading" : ""}*/}
                {/*>*/}
                {/*    <form onSubmit={this.onSubmit}>*/}
                {/*        <DialogTitle id="form-dialog-title">Report a problem</DialogTitle>*/}
                {/*        <DialogContent>*/}
                {/*            <DialogContentText>*/}
                {/*                Please describe the situation:*/}
                {/*            </DialogContentText>*/}
                {/*            <TextField*/}
                {/*                multiline rows={3}*/}
                {/*                autoFocus*/}
                {/*                margin="dense"*/}
                {/*                label="Description"*/}
                {/*                fullWidth*/}
                {/*                value={this.state.reportText}*/}
                {/*                onChange={(e)=>this.setState({reportText: e.target.value})}*/}
                {/*            />*/}
                {/*        </DialogContent>*/}
                {/*        <DialogActions>*/}
                {/*            <Button onClick={()=>this.props.closeDialog('report')}>Cancel</Button>*/}
                {/*            <Button type={"submit"}*/}
                {/*                    color="primary">*/}
                {/*                Send*/}
                {/*            </Button>*/}
                {/*        </DialogActions>*/}
                {/*    </form>*/}
                {/*</Dialog>*/}

                {/*{this.state.checkout && <Checkout /> }*/}
            </>
        );
    }
}


function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        showMessage    : Actions.showMessage,
    }, dispatch);
}

function mapStateToProps({dialogs}) {
    return {
        dialogs,
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(StartTime);


