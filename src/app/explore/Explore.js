import React from 'react';
// import Card from "@material-ui/core/Card";
// import { BaiduMap, Marker } from 'react-baidu-maps';
// import {withStyles} from "@material-ui/core/styles/index";
import ParkingList from "../parkings/ParkingList";
import {bindActionCreators} from "redux";
import * as Actions from "../../store/actions/actions";
import {connect} from "react-redux";
import Container from "../../UI/Container";
import Card from "../../UI/Card";
import P from "../../UI/P";


const styles = {
};
class Explore extends React.Component{
    state = {
        lot: null
    };

    componentDidMount =()=> {
        // console.log(this.props.history.listen(onpopstate));
        // window.onpopstate  = this.onBackHandler();
        // this.props.router.setRouteLeaveHook(this.props.route, this.routerWillLeave);
        this.props.getLots();
    };


    render() {
        const {classes} = this.props;
        return (
            <Container>
                <Card>
                    <P> Map </P>
                </Card>
                <Card>
                <ParkingList/>
                </Card>
            </Container>
        );
    }

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getLots : Actions.getLots
    }, dispatch);
}

function mapStateToProps({user, lots}) {
    return {
        lots                : lots,
    }
}
export default connect(mapStateToProps,mapDispatchToProps)(Explore);
