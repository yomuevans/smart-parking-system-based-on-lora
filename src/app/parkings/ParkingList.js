import React from "react";
import {View} from "react-native";
import {connect, useDispatch} from "react-redux";
import Empty from "../../UI/Empty";
import ListItem from "../../UI/ListItem";
import IconButton from "../../UI/IconButton";
import {NavigationActions, withNavigation} from 'react-navigation';

const ParkingList = props => {
    const {classes} = props;
    const {lots} = props;
    const dispatch = useDispatch();

    return (
        <View>
            {(!lots || lots.result.length === 0 )  ? <Empty />
                :lots.result.map(_lot =>
                <ListItem
                    onPress={() => dispatch(NavigationActions.navigate({routeName: 'lot',params: {id: _lot.id}}))}
                    title={_lot.name}
                    subtitle={_lot.capacity + " parking spot available"}
                    actions={<IconButton name={"jet"} />}
                    button key={_lot} />)
            }
        </View>
    )
};

function mapStateToProps({user, lots}) {
    return {
        user                : user,
        lots                : lots,
    }
}

export default withNavigation(connect(mapStateToProps)(ParkingList));
