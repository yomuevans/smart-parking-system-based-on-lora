import React, {Component} from 'react';
import {View} from "react-native";
import axios from "../../myaxios";
import Card from "../../UI/Card";
import IconButton from "../../UI/IconButton";
import Icon from "react-native-vector-icons/Ionicons";
import ListItem from "../../UI/ListItem";
import P from "../../UI/P";
import Button from "../../UI/Button";
import Divider from "../../UI/Divider";

class SingleLot extends Component {
    state = {
        lot: {}
    };

    // toStart = () => {
    //     this.props.history.push("/start");
    // };

    componentDidMount() {
        const id  = this.props.navigation.getParam('id');
        axios.get(`/lot/${id}`).then(res => {
            this.setState({lot: res.data});
        })
    }

    render() {
        const {classes} = this.props;
        const {lot} = this.state;
        return (
            <Card>
                <ListItem
                    title={lot.name}
                    subtitle={lot.capacity + " parking spot available"}
                    actions={<IconButton name={"jet"} />}
                    button key={lot} />
                <Card>
                    <ListItem
                        avatar={<Icon name={"md-pin"}/>}
                        title={lot.address}
                        subtitle={lot.city}
                        actions={<IconButton name={"jet"}/>}
                        button key={lot} />
                    <View style={{flexDirection:"row"}}>
                        <View>
                            <P type={"h6"}>{lot.capacity}</P>
                            <P type={"small"}>Spaces</P>
                        </View>
                        <View>
                            <P type={"h6"}>{lot.emptySpace}</P>
                            <P type={"small"}>Empty spaces</P>
                        </View>
                        <View>
                            <P type={"h6"}>{lot.chance}%</P>
                            <P type={"small"}>Your chance</P>
                        </View>
                    </View>
                    <Button>Navigate</Button>
                </Card>
                <Divider light/>
                <Button color={"primary"}>Report a problem</Button>
            </Card>
        );
    }
}


export default SingleLot;
