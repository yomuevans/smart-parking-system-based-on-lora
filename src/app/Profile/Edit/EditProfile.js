import React, {useState} from "react";
import {View} from "react-native";
import {useDispatch, useSelector} from "react-redux";
import Input from "../../../UI/Input";
import Button from "../../../UI/Button";
import axios, {baseURL} from "../../../myaxios";
import * as Actions from "../../../store/actions/actions";
import Container from "../../../UI/Container";
import Card from "../../../UI/Card";
import ActionsArea from "../../../UI/ActionsArea";
import Select from "../../../UI/Select";
import DPicker from "../../../UI/DPicker";
import Empty from "../../../UI/Empty";
import MainHeader from "../../../UI/MainHeader";

const EditProfile = () => {
    const user = useSelector(({user}) => user.user);
    const dispatch = useDispatch();
    const loading = useSelector(({loading}) => loading);

    const [formData, formDataSet] = useState({
        name:    user.name,
        gender:  user.gender,
        birth:   user.birth,
    });

    const onChange = name => val => {
        formDataSet({
            ...formData,
            [name]: val
        })
    };

    const submit = () => {
        dispatch(Actions.startLoading());
        axios.put(`${baseURL}/profile`, formData).then(() => {
            dispatch(Actions.showMessage({
                message: "Information saved!",
            }));
            dispatch(Actions.finishLoading());
        }).catch(er => {
            console.log(er.message);
        })
    };

    return (
        <>
            <Container hasActions>
                <MainHeader small title={"Edit Profile"}/>
                <Card>
                    {/*<Tabs>*/}
                    {/*    <Tab icon={"information-circle"} active>{"EditProfile"}</Tab>*/}
                    {/*    <Tab onPress={() => dispatch(NavigationActions.navigate({routeName: 'contact'}))}*/}
                    {/*         icon={"book"}>{"Contact"}</Tab>*/}
                    {/*    <Tab onPress={() => dispatch(NavigationActions.navigate({routeName: 'security'}))}*/}
                    {/*         icon={"lock"}>{"Security"}</Tab>*/}
                    {/*</Tabs>*/}
                    {loading ? <Empty/> :
                        <View style={{padding: 20}}>
                            <Input
                                label={"Name"}
                                value={formData.name}
                                onChangeText={onChange("Name")}
                                returnKeyType={"next"}/>
                            <Select
                                title={"Gender"}
                                items={["MALE", "FEMALE"]}
                                value={formData.gender}
                                onChange={onChange("gender")}/>
                            <DPicker
                                title={"Birthday"}
                                value={formData.birthday}
                                mode={"date"}
                                onChange={onChange("birthday")}/>
                        </View>
                    }
                </Card>
            </Container>
            <ActionsArea>
                <Button
                    color={"primary"} onPress={submit}>{"Send"}</Button>
            </ActionsArea>
        </>
    )
};

export default EditProfile;
