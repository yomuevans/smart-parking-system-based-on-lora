import React, {useState} from "react";
import {View} from "react-native";
import {NavigationActions, withNavigation} from "react-navigation";
import {useDispatch, useSelector} from "react-redux";
import Input from "../../UI/Input";
import Button from "../../UI/Button";
import axios, {userPrefix} from "../../myaxios";
import * as Actions from "../../store/actions/actions";
import Container from "../../UI/Container";
import Card from "../../UI/Card";
import Tabs from "../../UI/Tabs";
import Tab from "../../UI/Tab";
import ActionsArea from "../../UI/ActionsArea";
import moment from 'moment';
import Empty from "../../UI/Empty";

const defaultRules = {
    numbers: /^(([0-9]*)|(([0-9]*)\.([0-9]*)))$/,
    email: /^(\w)+(\.\w+)*@(\w)+((\.\w{2,3}){1,3})$/,
    required: /\S+/,
    date(format = "YYYY-MM-DD", value) {
        const d = moment(value, format);
        return !(d == null || !d.isValid());

    },
    minlength(length, value) {
        if (length === void (0)) {
            throw 'ERROR: It is not a valid length, checkout your minlength settings.';
        } else if (value.length > length) {
            return true;
        }
        return false;
    },
    maxlength(length, value) {
        if (length === void (0)) {
            throw 'ERROR: It is not a valid length, checkout your maxlength settings.';
        } else if (value.length > length) {
            return false;
        }
        return true;
    }
};

const defaultMessages = {
    en: {
        numbers: 'The field "{0}" must be a valid number.',
        email: 'The field "{0}" must be a valid email address.',
        required: 'The field "{0}" is mandatory.',
        date: 'The field "{0}" must be a valid date ({1}).',
        minlength: 'The field "{0}" length must be greater than {1}.',
        maxlength: 'The field "{0}" length must be lower than {1}.'
    },
};

const Security = (props) => {
    const dispatch = useDispatch();
    const loading = useSelector(({loading}) => loading);


    const form = {
        oldPassword: {
            required: true, minlength: 3, maxlength: 7,
            initValue: "",
        },
        newPassword: {
            required: true,
            initValue: "",
        },
        reEnterPassword: {
            required: true,
            initValue: "",
        },
    };

    let errors = [];
    const [formData, formDataSet] = useState({
        oldPassword: "",
        newPassword: "",
        reEnterPassword: "",
    });
    const onChange = name => val => {
        formDataSet({
            ...formData,
            [name]: val
        });
        //
    };

    const submit = () => {
        // if (!validate(form)) {
        // if (true) {
        //     console.log(errors);
        //     dispatch(Actions.showMessage({
        //         color: 'error',
        //         message: "There is an error in your form",
        //     }));
        //     return;
        // }
        // dispatch(Actions.startLoading());
        axios.put(`${userPrefix}/password`, formData).then(res => {
            dispatch(Actions.showMessage({
                message: "Password has changed!",
            }));
            dispatch(Actions.finishLoading());
            dispatch(Actions.setUserData());
        })
    };

    // Method to check if the field is in error
    const isFieldInError = (fieldName) => {
        console.log("isFieldInError", errors);
        return (errors.filter(err => err.fieldName === fieldName).length > 0);
    };

    // Method to return errors on a specific field
    const getErrorsInField = (fieldName) => () => {
        const foundError = errors.find(err => err.fieldName === fieldName);
        if (!foundError) {
            return []
        }
        return foundError.messages
    };

    const validate = (fields) => {
        // console.log("validate", fields);
        // Reset errors
        errors = [];
        // errorsSet([]);
        // Iterate over inner state
        for (const key of Object.keys(fields)) {
            // Check if child name is equals to fields array set up in parameters
            // console.log(key);
            const rules = fields[key];
            delete rules.initValue;
            // console.log(rules, key);
            if (rules) {
                // Check rule for current field
                _checkRules(key, rules, formData[key]);
            }
        }
        return errors.length === 0;
    };


    const _checkRules = (fieldName, rules, value) => {
        // console.log("_checkRules ", fieldName, rules, value);
        for (const key of Object.keys(rules)) {
            const isRuleFn = (typeof defaultRules[key] === "function");
            const isRegExp = (defaultRules[key] instanceof RegExp);
            // console.log(key, isRuleFn, isRegExp);
            if ((isRuleFn && !defaultRules[key](rules[key], value))
                || (isRegExp && !defaultRules[key].test(value))) {
                // console.log("there is an error", key);
                _addError(fieldName, key, rules[key], isRuleFn);
            }
        }
    };

    // [{ fieldName: "name", messages: ["The field name is required."] }]
    const _addError = (fieldName, rule, value, isFn) => {
        // console.log("_addError ", fieldName, rule, value,);
        const errMsg = defaultMessages["en"][rule].replace("{0}", fieldName).replace("{1}", value);
        let [error] = errors.filter(err => err.fieldName === fieldName);

        // error already exists
        // console.log(e, errors);

        // if (error) {
        //     // Update existing element
        //     const index = errors.indexOf(error);
        //
        //     e.messages.push(errMsg);
        //     e[index] = error;
        // } else {
        //     // Add new item
        //     e.push({
        //         fieldName,
        //         messages: [errMsg]
        //     });
        // }
        // errorsSet(e);

        if (error) {
            // Update existing element
            const index = errors.indexOf(error);
            error.messages.push(errMsg);
            errors[index] = error;
        } else {
            // Add new item
            errors.push({
                fieldName,
                messages: [errMsg]
            });
        }
    };

    // const formValidation = () => {
    //     const t = [];
    //     Object.keys(formData).map(item => {
    //         if (formData[item].trim() === ""){
    //             t.push("rule")
    //         }
    //     });
    //     return t;
    // };



    return (
        <>
            <Container hasActions>
                <Card>
                    <Tabs>
                        <Tab onPress={() => dispatch(NavigationActions.navigate({routeName: 'general'}))}
                             icon={"information-circle"}>General</Tab>
                        <Tab icon={"book"}
                             onPress={() => dispatch(NavigationActions.navigate({routeName: 'contact'}))}>Contact</Tab>
                        <Tab active icon={"lock"}>Security</Tab>
                    </Tabs>
                    {loading ? <Empty/> :
                        <View style={{padding: 20}}>
                            <Input
                                // error={isFieldInError("oldPassword")}
                                autoCorrect={false}
                                returnKeyLabel={"Next"}
                                returnKeyType={"next"}
                                label={"old Password"}
                                value={formData.oldPassword}
                                onChangeText={onChange("oldPassword")}/>
                            <Input
                                // error={isFieldInError("newPassword")}
                                secureTextEntry
                                autoCorrect={false}
                                returnKeyLabel={"Next"}
                                returnKeyType={"next"}
                                label={"new Password"}
                                value={formData.newPassword}
                                onChangeText={onChange("newPassword")}/>
                            <Input
                                secureTextEntry
                                // error={isFieldInError("reEnterPassword")}
                                autoCorrect={false}
                                returnKeyLabel={"Next"}
                                returnKeyType={"next"}
                                label={"reEnter Password"}
                                onSubmitEditing={submit}
                                value={formData.reEnterPassword}
                                onChangeText={onChange("reEnterPassword")}/>
                        </View>
                    }
                </Card>
            </Container>

            <ActionsArea disableCancel>
                <Button
                    disabled={
                        formData.oldPassword === "" ||
                        formData.newPassword === "" ||
                        formData.reEnterPassword === "" ||
                        formData.newPassword !== formData.reEnterPassword
                    }
                    color={"primary"}
                    onPress={submit}>Send</Button>
            </ActionsArea>
        </>
    )
};

export default withNavigation(Security);
