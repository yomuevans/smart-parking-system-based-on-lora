import React, {useContext} from "react";
import EditProfile from "./Edit/EditProfile";
import styled, {ThemeContext} from "styled-components";
import Button from "../../UI/Button";
import Orders from "./Orders/Orders";
import Cars from "./Cars/Cars";
import AddCar from "./Cars/AddCar";
import Settings from "./Settings/Settings";
import Transactions from "./Transactions/Transactions";
import {createSwitchNavigator} from "react-navigation";
import AboutUs from "./AboutUs";

const Styled = styled.View`
    background-color: ${props => props.theme.colors.bg};
    flex-direction: row;
    align-items: center;
`;

const Header = (props) => {
    const theme = useContext(ThemeContext);
    return (<Styled {...props} theme={theme}>
        <Button icon={"arrow-back"} onPress={() => props.navigation.goBack()}>Back to profile</Button>
    </Styled>)
};

const profileRoutes =
    createSwitchNavigator({
        Orders         : {screen: Orders},
        Cars           : {screen: Cars},
        AddCar         : {screen: AddCar},
        Transactions   : {screen: Transactions},
        Settings       : {screen: Settings},
        EditProfile    : {screen: EditProfile},
        AboutUs        : {screen: AboutUs}
    }, {
        navigationOptions: (props) => {
            // const themeContext = useContext(ThemeContext);
            return {
                title: "Back to profile",
                // headerTransparent: true,
                header: <Header {...props}/>,
                // headerStyle: true,
                // headerStyle: styles.header
            }
        },
        backBehavior: "none",
        tabBarComponent: props => null,
    });


export default profileRoutes;
