import React, {Component} from 'react';
import MainHeader from "../../UI/MainHeader";
import P from "../../UI/P";
import Card from "../../UI/Card";

class AboutUs extends Component {

    render() {
        const {classes} = this.props;
        return (
            <>
                <MainHeader title={"Smart Parking System"} subTitle={"based on LoRa WAN"}/>
                <Card>
                        {/*<div className="text-center">*/}
                            {/*<Icon  className={classes.logo}>local_convenience_store</Icon>*/}
                            {/*<P variant={"h4"}>Smart Parking System</P>*/}
                            {/*<P>base of <strong>LoRa</strong></P>*/}
                        {/*</div>*/}
                        {/*<Divider className={classes.margin}/>*/}
                        <P variant={"body1"}>Finding a vacant parking space in a congested area or a large
                            parking lot, especially, in peak hours, is always time consuming and frustrating to drivers.
                            It is common for drivers to keep circling a parking lot and look for a vacant parking space.
                            Real-time street parking availability information is important in urban areas, and if
                            available could reduce congestion, pollution, and gas consumption. </P>
                        <Divider />
                        <P>From Southwest Jiaotong University</P>
                        {/*<div className={"flex justify-between mt-1"}><P>Behrooz Erfanian </P><P*/}
                        {/*    color={"primary"}> +8615528073601</P></div>*/}
                </Card>

            </>
        );
    }
}


export default AboutUs;
