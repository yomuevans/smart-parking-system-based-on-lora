import React, {useEffect, useState} from "react";
import {FlatList, View} from "react-native";
// import Button from "../../../UI/Button";
import {NavigationActions} from "react-navigation";
import {useDispatch} from "react-redux";
import MainHeader from "../../../UI/MainHeader";
import IconButton from "../../../UI/IconButton";
import Card from "../../../UI/Card";
import Container from "../../../UI/Container";
import ListItem from "../../../UI/ListItem";
import {withTheme} from "styled-components";
import Empty from "../../../UI/Empty";
import {finishLoading, startLoading} from "../../../store/actions/loading.action";
import axios, {baseURL} from "../../../myaxios";
import {showMessage} from "../../../store/actions/message.actions";

const Cars = ({theme}) => {
    const [data, setData] = useState({
        metadata: {
            page: 0,
            totalItems: 0,
            totalPage: 0,
        },
        result: []
    });
    const dispatch = useDispatch();
    useEffect(() => {
        getData();
    }, []);


    const getData = () => {
        dispatch(startLoading());
        axios.get(`${baseURL}/profile/cars`).then(res => {
            dispatch(finishLoading());
            setData(res.data);
        }).catch(er=> {
            console.log(er);
        })
    };

    const deleteCar = (id) => {
        dispatch(startLoading());
        axios.delete(`${baseURL}/profile/cars/${id}`).then(res => {
            dispatch(finishLoading());
            dispatch(showMessage({color: "error",message: res.data.message}));
            getData();
        }).catch(er=> {
            console.log(er);
        })
    };

    return (
        <Container onRefresh={getData}>
            <MainHeader
                title={"Cars"}
                subTitle={`${data.metadata.totalItems} cars`}
                actions={
                    <View>
                        <IconButton onPress={() => dispatch(NavigationActions.navigate({routeName: 'AddCar'}))} name={"add"}/>
                    </View>
                }
            />
            <Card>
                {data.result.length > 0 ?
                    <FlatList
                        data={data.result}
                        renderItem={({item}) =>
                            <ListItem
                                button
                                onPress={() => {
                                    dispatch(NavigationActions.navigate({
                                        routeName: 'Lot',
                                        params: {id: item.parkingId}
                                    }))
                                }}
                                title={item.plateNumber}
                                actions={<IconButton size={40} onPress={()=>deleteCar(item.id)} name={"trash"}/>}
                                subtitle={item.color + " " + (item.brand || "") + " " + (item.modelYear || "" )}
                            />
                        }
                    />
                    :
                    <Empty/>
                }
            </Card>
        </Container>
    );
};

export default withTheme(Cars);
