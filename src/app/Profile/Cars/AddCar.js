import React, {useRef, useState} from "react";
import {Animated, Dimensions, StyleSheet} from "react-native"
import {useDispatch} from "react-redux";
import Input from "../../../UI/Input";
import Button from "../../../UI/Button";
import axios, {baseURL} from "../../../myaxios";
import {NavigationActions} from "react-navigation";
import Container from "../../../UI/Container";
import MainHeader from "../../../UI/MainHeader";
import Card from "../../../UI/Card";
import ActionsArea from "../../../UI/ActionsArea";
import P from "../../../UI/P";
import styled, {withTheme} from "styled-components";
import {showMessage} from "../../../store/actions/message.actions";

// export const headers = {
//     'Access-Control-Allow-Origin': '*',
//     'Content-Type': 'application/json',
//     'Authorization': 'Bearer $2a$10$I49vzP56RBiMpgWsAVz8Wep1diqELvofSsHXu0q391oJc6qQUN8hC',
// };

const AddCar = ({theme}) => {
    const dispatch = useDispatch();
    const [data, setData] = useState({
        "plateNumber": "",
        "modelYear": 0,
        "brand": "",
        "color": ""
    });
    // const [password, setPassword] = useState("");
    const [plateError, setPlateError] = useState(null);
    const [error, errorSet] = useState(null);
    const [anim, animSet] = useState(new Animated.Value(0));
    const [errorAnim, errorAnimSet] = useState(new Animated.Value(1));

    const [step, stepSet] = useState(0);
    // const passwordEl = useRef(null);
    const plateEl = useRef(null);
    const brandEl = useRef(null);

    const addCar = () => {
        const request = axios({
            method: 'post',
            url: `${baseURL}/profile/cars`,
            data
        });

        request.then((response) => {
            if (!response.data.error) {
                dispatch(NavigationActions.navigate({routeName: 'Cars'}));
                dispatch(showMessage({message: "Car successfully added", color: "success"}));
            }
        }).catch(err => {
            Animated.timing(errorAnim, {
                toValue: 1.2,
                duration: 50,
                useNativeDriver: true,
            }).start(()=>{
                Animated.spring(errorAnim, {
                    toValue: 1,
                    duration: 50,
                    useNativeDriver: true,
                }).start();
            });
            errorSet(err.response.data.message);
        });
    };

    const changeStep = (step) => {
        if (data.plateNumber.length > 5 || step === 0) {
            setPlateError(null);
            Animated.timing(anim, {
                toValue: step === 1 ? -Dimensions.get('window').width : 0,
                duration: 250,
                useNativeDriver: true,
            }).start(() => {
                if (step) brandEl.current.focus();
                else plateEl.current.focus();
                stepSet(step);
            });
        } else if (data.plateNumber.length < 7) {
            setPlateError("Plate number is too short");
        }
    };

    return (
        <>
            <Container hasActions>
                <MainHeader small title={"Add Car"} subTitle={"You can add 3 cars"}/>
                <Animated.View
                    style={{
                        flexDirection: "row",
                        width: "200%",
                        transform: [
                            {
                                translateX: anim
                            }
                        ]
                    }}
                >
                    <Card style={{paddingBottom: 10}}>
                        <BorderedView>
                            <P type={"h4"}>What's your plate number?</P>
                        </BorderedView>
                        <Input
                            error={!!plateError}
                            helperText={plateError}
                            onSubmitEditing={() => changeStep(1)}
                            enablesReturnKeyAutomatically
                            returnKeyLabel={"Next"}
                            returnKeyType={"next"}
                            maxLength={50}
                            autoCorrect={false}
                            ref={plateEl}
                            autoFocus={step === 0}
                            label={"Plate number"}
                            value={data.plateNumber}
                            onChangeText={(val) => setData({...data, plateNumber: val})}/>
                    </Card>

                    <Card style={{paddingBottom: 10}}>

                        {!!error &&
                        <Animated.View style={{transform: [{scale: errorAnim}]}}>
                            <P type={"h6"} align={"center"} color={"error"}>{error}</P>
                        </Animated.View>}
                        <Input
                            enablesReturnKeyAutomatically
                            returnKeyLabel={"Next"}
                            returnKeyType={"next"}
                            maxLength={50}
                            ref={brandEl}
                            label={"Brand"}
                            value={data.brand}
                            onChangeText={(val) => setData({...data, brand: val})}/>
                        <Input
                            enablesReturnKeyAutomatically
                            returnKeyLabel={"Next"}
                            returnKeyType={"next"}
                            maxLength={50}
                            ref={brandEl}
                            label={"Color"}
                            value={data.color}
                            onChangeText={(val) => setData({...data, color: val})}/>
                        <Input
                            enablesReturnKeyAutomatically
                            returnKeyLabel={"Send"}
                            onSubmitEditing={addCar}
                            returnKeyType={"send"}
                            maxLength={50}
                            ref={brandEl}
                            label={"Model Year"}
                            value={data.modelYear}
                            onChangeText={(val) => setData({...data, modelYear: val})}/>
                    </Card>
                </Animated.View>
            </Container>
            {step === 0 ?
                <ActionsArea disableCancel>
                    <Button color={"primary"} disabled={data.plateNumber.trim() === "" || data.plateNumber.trim().length < 7}
                            onPress={() => changeStep(1)}>Next</Button>
                </ActionsArea>
                :
                <ActionsArea disableCancel>
                    <Button onPress={() => changeStep(0)}>Back</Button>
                    <Button color={"primary"} disabled={data.brand.trim() === ""} onPress={addCar}>Add Car</Button>
                </ActionsArea>
            }
        </>
    )
};

const BorderedView = styled.View`
  margin-bottom: ${({theme}) => theme.margins.side * 2}px;
  padding: ${({theme}) => theme.margins.side * 2}px;
  border-bottom-width: ${StyleSheet.hairlineWidth};
  border-bottom-color: ${({theme}) => theme.colors.border};
`;

export default withTheme(AddCar);
