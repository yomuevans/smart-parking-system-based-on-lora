import React, {useEffect, useState} from "react";
import {Switch} from "react-native";
// import Button from "../../../UI/Button";
import {useDispatch} from "react-redux";
import MainHeader from "../../../UI/MainHeader";
import Card from "../../../UI/Card";
import Container from "../../../UI/Container";
import ListItem from "../../../UI/ListItem";
import {withTheme} from "styled-components";
import AsyncStorage from "@react-native-community/async-storage";

export const getSettings = async () => {
    try {
        return await AsyncStorage.getItem('sps:notification')
    } catch(e) {
        console.log('error in getting notification')
        // read error
    }
};

const Settings = ({theme}) => {
    const [notification, notificationSet] = useState(true);
    const dispatch = useDispatch();
    useEffect(() => {
        getData();
    }, []);

    const getData = () => {
        getSettings().then(n => {
            notificationSet(n === "true")
        })
        // dispatch(startLoading());
        // axios.get(`${baseURL}/profile/settings`).then(res => {
        //     dispatch(finishLoading());
        //     setData(res.data.result);
        // }).catch(er=> {
        //     console.log(er);
        // })
    };

    const updateSettings = (value) => {
        AsyncStorage.setItem('sps:notification', value.toString()).then(re=> {
            notificationSet(value)
        })

        // dispatch(startLoading());
        // const newData = [...data];
        // newData[data.findIndex(i => i.key === key)] = value;
        // axios.put(`${baseURL}/profile/settings`, newData).then(res => {
        //     dispatch(finishLoading());
        //     getData();
        // }).catch(er=> {
        //     console.log(er);
        // })
    };

    return (
        <Container onRefresh={getData}>
            <MainHeader
                title={"Settings"}
            />
            <Card>
                {/*{data.result.length > 0 ?*/}
                {/*    <FlatList*/}
                {/*        data={data.result}*/}
                {/*        renderItem={({item}) =>*/}
                {/*            <ListItem*/}
                {/*                // button*/}
                {/*                // onPress={() => {*/}
                {/*                //     dispatch(NavigationActions.navigate({*/}
                {/*                //         routeName: 'Lot',*/}
                {/*                //         params: {id: item.spot}*/}
                {/*                //     }))*/}
                {/*                // }}*/}
                {/*                title={item.key}*/}
                {/*                actions={<P type={"small"}>{item.value}</P>}*/}
                {/*            />*/}
                {/*        }*/}
                {/*    />*/}
                {/*    :*/}
                {/*    <Empty/>*/}
                {/*}*/}
                <ListItem
                    button
                    title={"Notification"}
                    actions={
                        <Switch value={notification} onValueChange={(val)=>updateSettings( val)}/>
                    }
                />
            </Card>
        </Container>
    );
};

export default withTheme(Settings);
