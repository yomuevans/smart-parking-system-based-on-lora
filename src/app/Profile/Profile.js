import React from "react";
import {Switch, View} from "react-native";
import {NavigationActions} from "react-navigation";
import {useDispatch, useSelector} from "react-redux";
import Button from "../../UI/Button";
import MainHeader from "../../UI/MainHeader";
import IconButton from "../../UI/IconButton";
import Container from "../../UI/Container";
import Card from "../../UI/Card";
import P from "../../UI/P";
import ListItem from "../../UI/ListItem";
import * as Actions from "../../store/actions/actions";
import {withTheme} from "styled-components";
import Avatar from "../../UI/Avatar";
import {showMessage} from "../../store/actions/message.actions";
import {logout} from "../../store/actions/user.actions";

const Profile = ({theme}) => {
    const dispatch = useDispatch();
    const onLogout = () => {
        dispatch(logout());
        dispatch(showMessage({color: "success",message:"Logout successful."}));
        dispatch(NavigationActions.navigate({routeName: 'Auth'}))
    };
    const user = useSelector(({user})=> user.user);
    const isLoggedIn = useSelector(({user})=> user.auth);
    const darkMode = useSelector(({theme})=> theme) === "dark";

    const checkForUpdate = () => {
        dispatch(showMessage({color: "success",message:"You are using the latest version."}))
    };

    return ( isLoggedIn ?
        <Container>
            <MainHeader
                actions={
                    <View>
                        <IconButton onPress={() => dispatch(NavigationActions.navigate({routeName: 'EditProfile'}))} name={"create"}/>
                    </View>
                }
            >
                <View style={{alignItems: "center"} }>
                    <Avatar size={70} source={{uri: user.avatarLink}}  style={{marginBottom: 10, marginTop: 10}}/>
                    <P type={"h1"}>{user.lastName} {user.mobile} </P>
                    <P type={"h4"} color={"textLight"}>{user.name} </P>
                </View>
            </MainHeader>
            <Card>
                <ListItem
                    noBorder
                    title={"Dark mode"}
                    actions={
                        <Switch trackColor={{true: theme.colors.primary, false: theme.colors.textLight}} value={darkMode} onValueChange={()=>dispatch(Actions.toggleDarkMode())} />
                    }
                />
            </Card>
            <Card>
                <ListItem
                    title={"Balance"}
                    actions={
                        <P color={"textLight"}>{user.balance || 0} RMB</P>
                    }
                />
                <ListItem
                    button
                    onPress={()=>dispatch(NavigationActions.navigate({routeName: 'Transactions'}))}
                    title={"Transactions"}
                />
                <ListItem
                    button
                    onPress={()=>dispatch(NavigationActions.navigate({routeName: 'Cars'}))}
                    title={"Cars"}
                />
                <ListItem
                    onPress={()=>dispatch(NavigationActions.navigate({routeName: 'Orders'}))}
                    button
                    noBorder
                    title={"Orders"}
                />
            </Card>
            <Card>
                <ListItem
                    button
                    onPress={()=>dispatch(NavigationActions.navigate({routeName: 'Settings'}))}
                    title={"Settings"}
                />
                <ListItem
                    button
                    onPress={()=>checkForUpdate()}
                    noBorder
                    title={"Check for update"}
                />
            </Card>
            <Button color={"error"} onPress={onLogout}>Logout</Button>
            <View style={{height: 20}} />
        </Container> :
            <Container>
                <MainHeader title={"Please login"} >
                    <View style={{alignItems: "center"} }>
                        <Button onPress={()=>dispatch(NavigationActions.navigate({routeName: 'Auth'}))} type={"h1"}>Login</Button>
                    </View>
                </MainHeader>
                <Card>
                    <ListItem
                        noBorder
                        title={"Dark mode"}
                        actions={
                            <Switch trackColor={{true: theme.colors.primary, false: theme.colors.textLight}} value={darkMode} onValueChange={()=>dispatch(Actions.toggleDarkMode())} />
                        }
                    />
                </Card>
                <Card>

                    <ListItem
                        button
                        onPress={()=>dispatch(NavigationActions.navigate({routeName: 'Auth'}))}
                        title={"Balance"}
                    />
                    <ListItem
                         onPress={()=>dispatch(NavigationActions.navigate({routeName: 'Auth'}))}
                        button
                        title={"Transactions"}
                    />
                    <ListItem
                         onPress={()=>dispatch(NavigationActions.navigate({routeName: 'Auth'}))}
                        button
                        title={"Cars"}
                    />
                    <ListItem
                         onPress={()=>dispatch(NavigationActions.navigate({routeName: 'Auth'}))}
                        button
                        noBorder
                        title={"Orders"}
                    />
                </Card>
                <View style={{height: 20}} />
            </Container>
    )
};


export default withTheme(Profile);
