import React, {useEffect, useState} from "react";
import {Animated, Easing, View} from "react-native";
import {useDispatch} from "react-redux";
import {finishLoading} from "../../store/actions/actions"
import {NavigationActions} from 'react-navigation';
import {baseURL} from "../../myaxios";
import axios from "axios";
import AsyncStorage from '@react-native-community/async-storage';
// import {setToken, setUserData} from "../../store/actions/user.actions";
import {withTheme} from "styled-components";
import P from "../../UI/P";
import {setUserData} from "../../store/actions/user.actions";

export const getToken = async () => {
    try {
        return await AsyncStorage.getItem('sps:token')
    } catch(e) {
        console.log('error in getting token')
        // read error
    }
};

const ScreenSplash = (props) => {
    // const theme = useTheme();

    const [anim, animSet] = useState(new Animated.Value(1));
    const dispatch = useDispatch();
    const changeStep = (step) => {
        Animated.timing(anim, {
            toValue: 1.5,
            duration: 250,
            useNativeDriver: true,
        }).start(() => {
            Animated.timing(anim, {
                toValue: 1,
                duration: 400,
                useNativeDriver: true,
                easing: Easing.quad
            }).start(() => {
                changeStep();
            });
        });
    };

    useEffect(()=>{
        changeStep();
        const tokenPromis = getToken();
        tokenPromis.then(token=>{
            const isTokenDefined = token !== undefined && token !== "" && token !== null;
            if(isTokenDefined) {
                console.info("Token is defend: ", token);
                axios.get(`${baseURL}/profile/checkToken`, {
                // axios.get(`${baseURL}/profile/current`, {
                    headers: {
                        authorization : "Bearer " + token
                    }
                }).then(res => {
                    console.log(res);
                    dispatch(finishLoading());
                    if(res.data.result) {
                        dispatch(setUserData(res.data.result));
                    }

                    // console.log("index.js | session is: " + sessionValid);
                    // switch (sessionValid) {
                    //     case "valid":
                    //         dispatch(NavigationActions.navigate({routeName: 'home'}));
                    //         dispatch(setUserData());
                    //         dispatch(setToken(token));
                    //         break;
                    //     case "invalid":
                    //         // store.dispatch(logoutUser());
                    //         break;
                    //     // case "refresh":
                    //     //     AsyncStorage.setItem("srx:token", res.data.token);
                    //     //     dispatch(setUserData());
                    //     //     dispatch(setToken(res.data.token));
                    //     //     dispatch(NavigationActions.navigate({routeName: 'Chat'}));
                    //     //     // dispatch(NavigationActions.navigate({routeName: 'App'}));
                    //
                    //         break;
                    //     default:
                    //     // console.log(sessionValid);
                    // }
                    dispatch(NavigationActions.navigate({routeName: 'home'}))
                }).catch(er => {
                    // dispatch(NavigationActions.navigate({routeName: 'Auth'}))
                    // store.dispatch(showMessage({message: "message.We can't connect to server now!"}));
                    console.log(er);
                });
            } else {
                console.info("Token is not defend");
                dispatch(NavigationActions.navigate({routeName: 'home'}))
            }
        });
    //
    });

    return (
        <View style={{backgroundColor: props.theme.colors.bg, flex: 1, justifyContent: "center", alignItems: "center"}}>
            <Animated.View
                style={{
                    transform: [
                        {
                            scale: anim
                        }
                    ]
                }}
            >
                <P type={"h1"} color={"primary"}>Smart Parking System</P>
            </Animated.View>
        </View>
    )
};

export default withTheme(ScreenSplash);
