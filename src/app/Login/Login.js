import React, {useRef, useState} from "react";
import {Animated, Dimensions, StyleSheet} from "react-native"
import {useDispatch, useSelector} from "react-redux";
import Input from "../../UI/Input";
import Button from "../../UI/Button";
import axios from "axios";
import {baseURL} from "../../myaxios";
import {NavigationActions} from "react-navigation";
import AsyncStorage from '@react-native-community/async-storage';
import {setUserData} from "../../store/actions/user.actions";
import Container from "../../UI/Container";
import MainHeader from "../../UI/MainHeader";
import Card from "../../UI/Card";
import ActionsArea from "../../UI/ActionsArea";
import P from "../../UI/P";
import styled, {withTheme} from "styled-components";
import {showMessage} from "../../store/actions/message.actions";
import Empty from "../../UI/Empty";
import {finishLoading, startLoading} from "../../store/actions/loading.action";

export const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    'Authorization': 'Bearer $2a$10$I49vzP56RBiMpgWsAVz8Wep1diqELvofSsHXu0q391oJc6qQUN8hC',
};

const Login = ({theme}) => {
    const dispatch = useDispatch();
    const [username, setUsername] = useState("");
    // const [loading, dispatch(startLoading] useState(false));
    const [password, setPassword] = useState("");
    const [usernameError, setUsernameError] = useState(null);
    const [error, errorSet] = useState(null);
    const [anim, animSet] = useState(new Animated.Value(0));
    const [errorAnim, errorAnimSet] = useState(new Animated.Value(1));
    const [step, stepSet] = useState(0);
    const passwordEl = useRef(null);
    const usernameEl = useRef(null);
    const loading = useSelector(({loading})=> loading);

    const login = () => {
        dispatch(startLoading());
        const request = axios({
            method: 'post',
            url: `${baseURL}/checkCode`,
            data: {
                mobile: username,
                confirmMobile: password
            },
            headers
        });

        request.then((response) => {
            if (!response.data.error) {
                dispatch(finishLoading());
                AsyncStorage.setItem("sps:token", response.data.result.token);
                AsyncStorage.setItem('sps:notification', true);
                dispatch(setUserData(response.data.result));
                dispatch(showMessage({message: "Login successful"}));
                dispatch(NavigationActions.navigate({routeName: 'home'}));
            } else {
                dispatch(finishLoading());

                dispatch(showMessage({message: "Login failed"}));
            }
        }).catch(err => {
            Animated.timing(errorAnim, {
                toValue: 1.2,
                duration: 50,
                useNativeDriver: true,
            }).start(()=>{
                Animated.spring(errorAnim, {
                    toValue: 1,
                    duration: 50,
                    useNativeDriver: true,
                }).start();
            });
            errorSet(err.response.data.message);
        });
    };

    const changeStep = (step) => {
        if (username.length > 5 || step === 0) {
            dispatch(startLoading());
            setUsernameError(null);
            const request = axios({
                method: 'post',
                url: `${baseURL}/getCode`,
                data: {
                    mobile: username,
                },
                headers
            });
            request.then(res=>{
                dispatch(finishLoading());
                dispatch(showMessage({message: res.data.message}));
                Animated.timing(anim, {
                    toValue: step === 1 ? -Dimensions.get('window').width : 0,
                    duration: 250,
                    useNativeDriver: true,
                }).start(() => {
                    if (step) passwordEl.current.focus();
                    else usernameEl.current.focus();
                    stepSet(step);
                });
            }).catch(er=>{
                if(er.data.message) {
                    dispatch(showMessage({message: er.data.message}));
                }
                console.log(er)
            })

        } else if (username.length < 10) {
            setUsernameError("Username is too short");
        }
    };

    return (
        <>
            <Container hasActions>
                <MainHeader small title={"Login"} subTitle={"Smart Parking System"}/>
                <Animated.View
                    style={{
                        flexDirection: "row",
                        width: "200%",
                        transform: [
                            {
                                translateX: anim
                            }
                        ]
                    }}
                >

                    <Card style={{paddingBottom: 10}}>
                            <BorderedView>
                                <P type={"h4"}>What's your username?</P>
                            </BorderedView>
                            {loading ? <Empty/> : <>
                            <Input
                                error={!!usernameError}
                                helperText={usernameError}
                                onSubmitEditing={() => changeStep(1)}
                                enablesReturnKeyAutomatically
                                returnKeyLabel={"Next"}
                                returnKeyType={"next"}
                                maxLength={50}
                                autoCorrect={false}
                                ref={usernameEl}
                                autoFocus={step === 0}
                                label={"Username"}
                                textContentType={'username'}
                                value={username}
                                onChangeText={(val) => setUsername(val)}/>
                        </>
                        }
                    </Card>

                    <Card style={{paddingBottom: 10}}>
                        <BorderedView>
                            <P type={"h4"}>Just verify your password!</P>
                        </BorderedView>
                        {loading ? <Empty/> : <>
                        {!!error &&
                        <Animated.View style={{transform: [{scale: errorAnim}]}}>
                            <P type={"h6"} align={"center"} color={"error"}>{error}</P>
                        </Animated.View>}
                        <Input
                            error={!!error}
                            enablesReturnKeyAutomatically
                            onSubmitEditing={login}
                            returnKeyLabel={"Send"}
                            returnKeyType={"send"}
                            maxLength={50}
                            secureTextEntry
                            ref={passwordEl}
                            textContentType={'password'}
                            label={"Password"}
                            value={password}
                            onChangeText={(val) => setPassword(val)}/>
                        </>
                        }
                    </Card>
                </Animated.View>
            </Container>
            {step === 0 ?
                <ActionsArea disableCancel>
                    <Button color={"primary"} disabled={username.trim() === "" || username.length < 10}
                            onPress={() => changeStep(1)}>Next</Button>
                </ActionsArea>
                :
                <ActionsArea disableCancel>
                    <Button onPress={() => changeStep(0)}>Back</Button>
                    <Button color={"primary"} disabled={password.trim() === ""} onPress={login}>Login</Button>
                </ActionsArea>
            }
        </>
    )
};

const BorderedView = styled.View`
  margin-bottom: ${({theme}) => theme.margins.side * 2}px;
  padding: ${({theme}) => theme.margins.side * 2}px;
  border-bottom-width: ${StyleSheet.hairlineWidth};
  border-bottom-color: ${({theme}) => theme.colors.border};
`;

export default withTheme(Login);
