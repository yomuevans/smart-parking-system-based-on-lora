import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as Actions from "../../store/actions/actions";
import Card from "../../UI/Card";
import ParkingList from "../parkings/ParkingList";
import MainHeader from "../../UI/MainHeader";
import IconButton from "../../UI/IconButton";
import Input from "../../UI/Input";
import Container from "../../UI/Container";

class HomePage extends Component {
    componentDidMount = () => {
        this.props.getLots();
    };

    render() {
        const {classes, lots} = this.props;
        return (
            <Container>
                <MainHeader
                    title={"Smart Parking System"}
                    subTitle={"Your location"}
                    actions={<>
                        <Input label={"Search name"} style={{flex: 1}} />
                        <IconButton
                            onPress={() => this.props.navigation.navigate('ScanQR')} name={"expand"} />
                    </>}
                />
                {/*<Card>*/}
                    {/*{!!lots.metadata.totalItems &&*/}
                    {/*<P variant={"subtitle1"}>{lots.metadata.totalItems} Parking lots found!</P>}*/}
                    {/*<CardContent>*/}
                        {/*<BaiduMap mapContainer={<div style={{ height: '200px' }} />}>*/}
                        {/*<Marker position={{ lng: 11576266, lat: 3581455 }} />*/}
                        {/*</BaiduMap>*/}
                    {/*</CardContent>*/}
                {/*</Card>*/}
                <Card>
                    <ParkingList/>
                </Card>
            </Container>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        getLots : Actions.getLots
    }, dispatch);
}

function mapStateToProps({user, lots}) {
    return {
        user                : user,
        lots                : lots,
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
