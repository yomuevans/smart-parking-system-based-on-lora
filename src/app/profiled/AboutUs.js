import React, {Component} from 'react';
import {withStyles} from "@material-ui/core/styles/index";
import Typography from "@material-ui/core/Typography";
import Divider from '@material-ui/core/Divider';
import Icon from "@material-ui/core/Icon";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card/Card";

;

const styles = theme => ({
    margin: {margin: "15px 0"},
    lineHeight: {lineHeight: 2},
    logo: {
        color: theme.palette.primary.main,
        margin: "10px 0",
        fontSize: 150
    }
});

class AboutUs extends Component {

    render() {
        const {classes} = this.props;
        return (
            <>
                <AppBar position="static">
                    <Toolbar disableGutters>
                        <IconButton onClick={this.props.history.goBack} color="inherit">
                            <Icon>chevron_left</Icon>
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            AboutUs
                        </Typography>
                    </Toolbar>
                </AppBar>

                <Card>
                    <CardContent className={classes.lineHeight}>
                        <div className="text-center">
                            <Icon  className={classes.logo}>local_convenience_store</Icon>
                            <Typography variant={"h4"}>Smart Parking System</Typography>
                            <Typography>base of <strong>LoRa</strong></Typography>
                        </div>
                        <Divider className={classes.margin}/>
                        <Typography variant={"body1"}>Finding a vacant parking space in a congested area or a large
                            parking lot, especially, in peak hours, is always time consuming and frustrating to drivers.
                            It is common for drivers to keep circling a parking lot and look for a vacant parking space.
                            Real-time street parking availability information is important in urban areas, and if
                            available could reduce congestion, pollution, and gas consumption. </Typography>
                        <Divider  className={classes.margin}/>
                        <Typography>From <strong>Southwest Jiaotong University</strong></Typography>
                        <div className={"flex justify-between mt-1"}><Typography>Behrooz Erfanian </Typography><Typography
                            color={"primary"}> +8615528073601</Typography></div>
                    </CardContent>
                </Card>

            </>
        );
    }
}

AboutUs.propTypes = {};

export default withStyles(styles)(AboutUs);
