import React, {Component} from 'react';

import {withStyles} from "@material-ui/core/styles/index";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import Icon from "@material-ui/core/Icon";
import Divider from "@material-ui/core/Divider";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Gray from "@material-ui/core/colors/grey";
import {bindActionCreators} from "redux";
import * as Actions from "../../store/actions/actions";
import {connect} from "react-redux";
import TopUp from "./settings/TopUp";
import Withdraw from "./settings/Withdraw";

const styles = theme => ({
    colorDisabled: {
        color: Gray["600"],
        fontWeight: 'lighter',
    }
});
class Wallet extends Component {
    state = {
        balance: 1522
    };

    render() {
        const {classes} = this.props;
        return (
            <>
                <TopUp />
                <Withdraw/>
                <AppBar position="static">
                    <Toolbar disableGutters>
                        <IconButton onClick={this.props.history.goBack} color="inherit">
                            <Icon>chevron_left</Icon>
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            Wallet
                        </Typography>
                    </Toolbar>
                </AppBar>
                <Card>
                    <Divider  light/>
                    <CardContent>
                        <Typography   variant={"subtitle1"}>
                            balance
                        </Typography>
                        <Typography   variant={"display3"}>
                            <Icon>attach_money</Icon>{this.state.balance}
                        </Typography>
                    </CardContent>
                    <List disablePadding>
                        <ListItem button className={classes.listItem} onClick={()=>this.props.openDialog('topUp')}>
                            <ListItemText classes={{primary: classes.colorDisabled}}>Top up</ListItemText>
                            <ListItemSecondaryAction>
                                <Icon color={'disabled'}>chevron_right</Icon>
                            </ListItemSecondaryAction>
                        </ListItem>
                        <Divider light component={'li'}/>
                        <ListItem button className={classes.listItem}  onClick={()=>this.props.openDialog('withdraw')}>
                            <ListItemText classes={{primary: classes.colorDisabled}}>Withdraw</ListItemText>
                            <ListItemSecondaryAction>
                                <Icon color={'disabled'}>chevron_right</Icon>
                            </ListItemSecondaryAction>
                        </ListItem>
                        <Divider light component={'li'}/>
                    </List>
                </Card>
            </>
        );
    }

}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        openDialog    : Actions.openDialog,
    }, dispatch);
}
export default withStyles(styles)(connect(null,mapDispatchToProps)(Wallet));
