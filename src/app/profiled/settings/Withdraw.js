import React, {Component} from 'react';
import {connect} from 'react-redux';
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import TextField from "@material-ui/core/TextField";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import {bindActionCreators} from "redux";
import * as Actions from "../../../store/actions/actions";
import axios from "../../../myaxios";

class Withdraw extends Component {

    state = {
        passwordDialog: false,
        loading: false,
        password: "",
        value: ""
    };

    onSubmit = (e) => {
        e.preventDefault();
        axios.put("/withdraw", {amount: this.state.value, password: this.state.password}).then(res => {
            this.props.closeDialog("withdraw");
            // todo show message
            this.setState({passwordDialog: false})
        })
    };

    render() {
        return (
            <>
                <Dialog
                    open={this.props.dialogs['withdraw'] === true}
                    onClose={() => this.props.closeDialog("withdraw")}
                    aria-labelledby="form-dialog-title"
                    fullWidth
                    className={this.state.loading ? "loading" : ""}
                >
                    <DialogTitle id="form-dialog-title">Amount</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Less than 200
                        </DialogContentText>
                        <TextField
                            autoFocus
                            margin="dense"
                            label="Amount"
                            className={"text-center"}
                            fullWidth
                            value={this.state.value}
                            onChange={(e) => this.setState({value: e.target.value})}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={() => this.props.closeDialog('withdraw')}>Cancel</Button>
                        <Button onClick={()=>this.setState({passwordDialog: true})}
                                color="primary">
                            Withdraw
                        </Button>
                    </DialogActions>
                </Dialog>
                <Dialog
                    open={this.state.passwordDialog}
                    onClose={()=>this.setState({passwordDialog: false})}
                    aria-labelledby="form-dialog-title"
                    fullWidth
                    className={this.state.loading ? "loading" : ""}
                >
                    <DialogTitle id="form-dialog-title">Password</DialogTitle>
                    <DialogContent>
                        <TextField
                            autoFocus
                            margin="dense"
                            label="Password"
                            fullWidth
                            value={this.state.password}
                            onChange={(e) => this.setState({password: e.target.value})}
                        />
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.onSubmit} disabled={this.state.password.length < 4}
                                color="primary">
                            Send
                        </Button>
                    </DialogActions>
                </Dialog>
            </>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        openDialog: Actions.openDialog,
        closeDialog: Actions.closeDialog
    }, dispatch);
}

function mapStateToProps({dialogs}) {
    return {dialogs};
}

export default connect(
    mapStateToProps, mapDispatchToProps
)(Withdraw);
