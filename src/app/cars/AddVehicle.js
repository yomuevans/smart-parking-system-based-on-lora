import React, {Component} from 'react';
import {withStyles} from "@material-ui/core/styles/index";
import AppBar from "@material-ui/core/AppBar";
import classNames from 'classnames';
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import axios from "../../myaxios"
import {bindActionCreators} from "redux";
import * as Actions from "../../store/actions/actions";
import {connect} from "react-redux";

const styles = theme => ({
    button: {
        display: 'block',
        width: '60%',
        margin: 'auto',
        height: 50,
        marginTop: 40,
    },
    header: {
        margin: 20
    },
    bootstrapRoot: {
        padding: 0,
        width: '100%',
        // background: 'white',
        'label + &': {
            marginTop: theme.spacing.unit * 3,
        },
    },
    bootstrapInput: {
        border: '1px solid '+theme.palette.primary.main+'  !important',
        borderRadius: 3,
        fontSize: "20px !important",
        background: 'white !important' ,
        padding: '5px 15px  !important',
        width: 'calc(100% - 24px)',
        height: 50,
    },
    bootstrapFormLabel: {
        fontSize: 18,
    },
    input:{
        marginTop: 25
    }
});

class AddVehicle extends Component {
    state = {
        checkedA: false,
        disableButton: true,
        plate: "",
        brand: "",
        modelYear: "",
        color: "",
        step: 0,
        error: false

    };

    handleCheck = name => event => {
        this.setState({ [name]: event.target.checked });
        const temp = this.state.checkedA;
        this.setState({disableButton: temp})
    };

    checkPlate = () => {
        if(this.state.plate.length < 5)
            this.setState({error: true});
        else
            this.setState({step: 1})
    };

    handleChange = name => event => {
        this.setState({ [name]: event.target.value });
    };

    onSubmit = () => {

        const data = {
            "plate": this.state.plate,
            "modelYear": this.state.modelYear,
            "brand": this.state.brand,
            "color": this.state.color
        };
        const request = (this.props.match.params.carId) ? axios.put('/car', {...data, id: this.props.match.params.carId}) : axios.post('/car', data);
        request.then(res => {
            this.props.showMessage({message:"Vehicle just added!"});
            this.props.history.push("/user/vehicles")
        })
    };

    render() {
        const {classes} = this.props;
        return (
            <div>
                <AppBar position="static">
                    <Toolbar disableGutters>
                        <IconButton onClick={this.props.history.goBack} color="inherit">
                            <Icon>chevron_left</Icon>
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            Add Vehicle
                        </Typography>
                    </Toolbar>
                </AppBar>
                <CardContent>
                        {!this.state.step ?
                            <>
                                <Typography variant="subtitle1" className={classNames('light text-center', classes.header)}
                                            >
                                    Add your plate number carefully
                                </Typography>
                                <TextField
                                    onChange={this.handleChange('plate')}
                                    fullWidth
                                    value={this.state.plate}
                                    id="bootstrap-input"
                                    placeholder={"Plate Number"}
                                    InputProps={{
                                       disableUnderline: true,
                                       classes: {
                                           root: classes.bootstrapRoot,
                                           input: classes.bootstrapInput,
                                       }
                                    }}
                                    InputLabelProps={{
                                       shrink: true,
                                       className: classes.bootstrapFormLabel,
                                    }}>
                                </TextField>
                                <FormControlLabel
                                    control={
                                        <Checkbox
                                            checked={this.state.checkedA}
                                            onChange={this.handleCheck('checkedA')}
                                            value="checkedA" color="primary"
                                        />
                                    }
                                    label="I accept terms and conditions"
                                />
                                {this.state.error && <div className={"bg-red-light p-3 border-red text-white rounded flex items-center"}><Icon className={"mr-3"}>error</Icon> Your plate number is wrong!</div>}
                                <Button fullWidth onClick={this.checkPlate}
                                        disabled={this.state.disableButton} size={"large"} className={classes.button}
                                        variant={"contained"} color={"primary"}>Next Step</Button>
                            </>
                            :
                            <>
                                <Typography variant="subtitle1" className={classNames('light', classes.header)}
                                             >
                                    Add your other information
                                </Typography>
                                <div className={"px-10"}>
                                    <TextField
                                        onChange={this.handleChange('brand')}
                                        fullWidth
                                        value={this.state.brand}
                                        placeholder={"Brand"}/>
                                    <TextField
                                        className={classes.input}
                                        type={"date"}
                                        min={1980} max={2022}
                                        onChange={this.handleChange('modelYear')}
                                        fullWidth
                                        value={this.state.modelYear}
                                        placeholder={"Year"}/>
                                    <Select
                                        className={classes.input}
                                        value={this.state.color}
                                        onChange={this.handleChange('color')}
                                        fullWidth
                                        placeholder={"Color"}>
                                        <MenuItem value="">
                                            <em>None</em>
                                        </MenuItem>
                                        <MenuItem value={"white"}>White</MenuItem>
                                        <MenuItem value={"grey"}>Grey</MenuItem>
                                        <MenuItem value={"blue"}>Blue</MenuItem>
                                        <MenuItem value={"green"}>Green</MenuItem>
                                        <MenuItem value={"bronze"}>Bronze</MenuItem>
                                    </Select>
                                    <Button fullWidth onClick={this.onSubmit}
                                            disabled={this.state.disableButton} size={"large"} className={classes.button}
                                            variant={"contained"} color={"primary"}>Next Step</Button>
                                </div>
                            </>
                        }
                </CardContent>

            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        showMessage    : Actions.showMessage,
    }, dispatch);
}

export default withStyles(styles)(connect(null,mapDispatchToProps)(AddVehicle));
