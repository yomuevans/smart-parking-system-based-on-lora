import React, {Component} from 'react';
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import IconButton from "@material-ui/core/IconButton";
import Icon from "@material-ui/core/Icon";
import {withStyles} from "@material-ui/core/styles/index";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Link from "react-router-dom/Link";
import axios from "../../myaxios"
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction/ListItemSecondaryAction";
import {bindActionCreators} from "redux";
import * as Actions from "../../store/actions/actions";
import {connect} from "react-redux";


const styles = theme => ({
    button: {
        display: 'block',
        border: '3px dashed #eee',
        width: '100%',
        height: 100,
        marginTop: 20,
        textAlign: 'center',
        lineHeight: '45px'
    }
});

class Vehicles extends Component {
    state = {
        data: [],
        loading: true
    };

    componentDidMount = () => {
        axios.get('/cars').then(res => this.setState({data: res.data, loading: false}))
    };
    render() {
        const {classes} = this.props;
        return (
            <div>
                <AppBar position="static">
                    <Toolbar disableGutters>
                        <IconButton onClick={this.props.history.goBack} color="inherit">
                            <Icon>chevron_left</Icon>
                        </IconButton>
                        <Typography variant="h6" color="inherit">
                            Vehicles
                        </Typography>
                    </Toolbar>
                </AppBar>

                    {this.state.data.length === 0 ?
                        <CardContent className={this.state.loading ? "loading " : ""}>
                            <Typography variant={"subtitle1"}>
                                You can add up to 3 vehicles
                            </Typography>
                            <Button component={Link} to={"/user/vehicles/add"} size={"large"} className={classes.button}
                                    color={"primary"}>
                                <Icon>add</Icon> <Typography variant={"h6"} color={"inherit"}>Add
                                Vehicle</Typography>
                            </Button>
                        </CardContent>
                        :
                        <List className={"bg-white"}>
                            {this.state.data.map((_i, i) =>
                                <ListItem key={i}>
                                    <ListItemText
                                        primary={_i.brand}
                                        secondary={_i.plate}
                                    />
                                    <ListItemSecondaryAction>
                                        <IconButton component={Link} to={`/car/${_i.id}`}>
                                            <Icon>edit</Icon>
                                        </IconButton>
                                        <IconButton  onClick={()=>{
                                            this.props.deleteConfirm("/car", _i.id, _i.brand + " (" + _i.plate + ")")
                                        }}>
                                            <Icon>delete</Icon>
                                        </IconButton>
                                    </ListItemSecondaryAction>
                                </ListItem>
                            )}
                        </List>
                    }
            </div>
        );
    }
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({
        deleteConfirm    : Actions.deleteConfirm,
        showMessage    : Actions.showMessage,
    }, dispatch);
}

export default withStyles(styles)(connect(null,mapDispatchToProps)(Vehicles));

