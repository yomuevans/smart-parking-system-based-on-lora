export const TOGGLE_DARK_MODE = "[THEME] TOGGLE_DARK_MODE";

export const toggleDarkMode = () => {
    return {
        type: TOGGLE_DARK_MODE,
    }
};
