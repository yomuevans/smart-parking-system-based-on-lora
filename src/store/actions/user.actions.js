import axios from "../../myaxios";
import {finishLoading, startLoading} from "./loading.action";
import AsyncStorage from "@react-native-community/async-storage";

export const LOGIN = '[USER] LOGIN';
export const LOGOUT = '[USER] LOGOUT';
// export const SET_USER_DATA = 'SET_USER_DATA';

// export const login = (mobile, code) => {
//     return dispatch => {
//         dispatch(startLoading());
//         axios.post("/checkCode", {mobile, code}).then(res=> {
//             dispatch(finishLoading());
//             // localStorage.setItem("token", res.data.token);
//             dispatch({type: LOGIN, info:res.data});
//         })
//     }
// };

export const logout = () => {
    return dispatch => {
        // todo put loading
        // axios.get("/logoutLog").then(res=> {
            AsyncStorage.removeItem('sps:token');
            dispatch({type: LOGOUT});
        // })
    }
};

export const setUserData = (data) => {
    return {type: LOGIN, data}
};
