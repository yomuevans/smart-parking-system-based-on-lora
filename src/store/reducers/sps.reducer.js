import * as Actions from '../actions/actions';

const initialState = {
    state  : null,
    options: {
        anchorOrigin    : {
            vertical  : 'bottom',
            horizontal: 'left'
        },
        autoHideDuration: 5000,
        message         : "Hi"
    },
};

const sps = function (state = initialState, action) {
    switch ( action.type )
    {
        case Actions.DELETE_CONFIRMATION: {
            return {
                ...state,
                requestLink: action.requestLink, id: action.id, title: action.title
            }
        }
        case Actions.SHOW_MESSAGE: {
            return {
                ...state,
                state  : true,
                options: {
                    ...state.options,
                    anchorOrigin    : {
                        ...state.options.anchorOrigin,
                        ...action.options.anchorOrigin
                    },
                    ...action.options
                }
            };
        }
        case Actions.HIDE_MESSAGE: {
            return {
                ...state,
                state: null
            };
        }
        default: {
            return state;
        }
    }
};

export default sps;

