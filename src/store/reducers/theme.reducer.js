import * as Actions from '../actions/actions';


const themeReducer = function (state = "dark", action) {
    switch ( action.type )
    {
        case Actions.TOGGLE_DARK_MODE: {
            return state === "dark" ? "light" : "dark";
        }
        default: {
            return state;
        }
    }
};

export default themeReducer;
