import {combineReducers} from 'redux';
// import dialogs from './dialogs.reducer';
import loading from "./loading.reducer"
// import lots from "./lots.reducer";
import user from "./user.reducer";
// import sps from "./sps.reducer";
import {createNavigationReducer} from 'react-navigation-redux-helpers';
import Router from "../../Router";
import theme from "./theme.reducer";
import message from "./message.reducer";

const navReducer = createNavigationReducer(Router);

// export const nav = (state = initNavState,action) => {
//         const nextState = RootNav.router.getStateForAction(action, state);
//         return nextState || state;
// };

const appReducer = combineReducers({
        nav: navReducer,
        loading,
        message,
        // chat,
        user,
        // task,
        theme,
        // srx,
});

export default appReducer;
