import * as Actions from '../actions/actions'

const initialState = {
    user: {
        avatarLink: null,
        balance: null,
        confirmMobile: "",
        createdAt: 1569334190000,
        email: null,
        id: 132,
        mobile: "1111111111",
        name: null,
        orderCount: null,
        paidStatus: false,
        plateNumber: null,
        status: true,
        type: 1,
        updatedAt: null,
    },
    token: null,
    auth: false,
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case Actions.LOGOUT : {
            return {
                initialState
            }
        }
        case Actions.LOGIN : {
            return {
                ...state,
                ...action.data,
                auth: true
            }
        }
        default: return state;
    }
};

export default reducer;
