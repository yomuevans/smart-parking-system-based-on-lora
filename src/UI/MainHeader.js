import React, {Component, useEffect, useState} from "react";
import {Text, View, Alert, Modal, TouchableHighlight, Switch, StyleSheet, Animated} from "react-native";
import {withNavigation} from 'react-navigation';
import {connect, useDispatch, useSelector} from "react-redux";
import {withTheme} from "styled-components";
import P from "./P";
import * as Actions from "../store/actions/actions";
import {bindActionCreators} from "redux";
import styled from "styled-components/native";
import PropTypes from "prop-types";

const mainHeight = -200;

class MainHeader extends Component {
    // state = {
    //     translateY: 0,
    //     position: "bottom"
    // };

    // componentDidUpdate = (prevProps, prevState) => {
    //     const {y, scrolledState} = this.props;
    //     const {translateY, position} = this.state;
    //     // console.log(translateY, position, y);
    //     if (prevProps.y !== y) {
    //         if (!scrolledState && !prevProps.scrolledState) {
    //             if (y < (200)) { // swipe down
    //                 this.setState({translateY: -(y)});
    //             }
    //             // if (y > -(mainHeight-60) && y < 0 &&  position === "bottom") { // swipe up
    //             //     this.setState({top: k + y});
    //             // }
    //
    //         }
    //     }
    //     if ((prevProps.scrolledState !== scrolledState && scrolledState)
    //     // && !(position === "bottom" && y < (mainHeight-60) && y > 0)
    //     // && !(position === "top" && y > -(mainHeight-60) && y < 0)
    //     ) {
    //         if (y < 10) {
    //             this.setState({position: "bottom", translateY: 0,})
    //         } else if (y > 10) {
    //             this.setState({position: "top", translateY: -200})
    //         }
    //     }
    //     // }
    //     // if (prevProps.y !== y) {
    //     //     if (!scrolledState && !prevProps.scrolledState) {
    //     //         if (y < (mainHeight-60) && y > 0 &&  position === "top") { // swipe down
    //     //                  this.setState({height: k + y});
    //     //         }
    //     //         if (y > -(mainHeight-60) && y < 0 &&  position === "bottom") { // swipe up
    //     //                  this.setState({height: k + y});
    //     //         }
    //     //
    //     //     }
    //     // }
    //     // if((prevProps.scrolledState !== scrolledState && scrolledState)
    //     //     && !(position === "bottom" && y < (mainHeight-60) && y > 0)
    //     //     && !(position === "top" && y > -(mainHeight-60) && y < 0)
    //     //     ) {
    //     //     if(position === "bottom" && y < (mainHeight-60) && y > 0) {
    //     //         console.log("youre swiping down for nothing");
    //     //     }
    //     //     if(position === "top" && y > -(mainHeight-60) && y < 0) {
    //     //         console.log("youre swiping up for nothing");
    //     //     }
    //     //     if (height > 150) {
    //     //         this.setState({position: "bottom", height: mainHeight, k: mainHeight})
    //     //     } else if (height < 150) {
    //     //         this.setState({position: "top", height: 60, k: 60})
    //     //     }
    //     //     this.props.scrolled(false)
    //     // }
    //     if (prevProps.scrolledState !== scrolledState && scrolledState) {
    //         this.props.scrolled(false)
    //     }
    // };

    render() {
        // const {translateY} = this.state;
        const props = this.props;
        // const op = (translateY * (1 / 200) + 1);
        return (
            <BigHeader {...props}>
                <View style={{alignItems: "center", justifyContent: "center", flex: 1}}>
                    {props.children || <View>
                        <P type={"h1"} align={"center"} color={props.titleColor} style={{marginBottom:5}}>{props.title}</P>
                        <P type={"h4"} align={"center"} color={"textLight"}>{props.subTitle}</P>
                    </View>}
                </View>
                <SmallHeader theme={props.theme}>
                    {props.actions}
                </SmallHeader>
            </BigHeader>
        )
    }
}

const BigHeader = styled.View`
  background-color: ${props => props.theme.colors.bg};
  height: ${props => props.small ? 140 : 260}px
`;

const SmallHeader = styled.View`
   align-items: center;
   justify-content: flex-end;
   flex-direction: row;
   margin-right: ${props => props.theme.margins.side}
`;


MainHeader.propTypes = {
    small: PropTypes.bool,
    title: PropTypes.string,
    subTitle: PropTypes.string,
    actions: PropTypes.element
};


export default withTheme(withNavigation(MainHeader));
