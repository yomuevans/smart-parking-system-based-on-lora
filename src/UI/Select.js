import React from "react";
import {
    View,
    StyleSheet, Picker,
} from "react-native";
import styled from "styled-components";
import PropTypes from "prop-types";
import {withTheme} from "styled-components";
import P from "./P";

const BaseSelect = styled.View`
    margin-bottom: ${props => props.theme.margins.side * 2}px;
    border-bottom-width: 1;
    margin-right: ${({theme}) => theme.margins.side*2}px;
    margin-left: ${({theme}) => theme.margins.side*2}px;
    margin-bottom: ${({theme}) => theme.margins.side*3}px;
    border-bottom-color: ${(props) => props.error ? props.theme.colors.error : props.theme.colors.textLight};
`;

const Select = props => {
    return (
        <BaseSelect
            error={props.error !== null && props.error !== undefined}
            required={props.required}
            disabled={props.disabled}>
            <P color={"textLight"}>{props.title}</P>
            <Picker
                selectedValue={props.value || "choose"}
                style={{borderBottomWidth: 0, color: props.theme.colors.text}}
                onValueChange={(val) => props.onChange(val)}>
                <Picker.Item label={"Choose one"} value={"choose"}/>
                {props.items !== undefined ? props.items.map((item, i) => (
                    <Picker.Item label={item} key={i} value={item}/>
                )) : null}
            </Picker>
            {props.error && <P color={"error"}>{props.error}</P>}
            {props.helper !== undefined ? <P color={"textLight"}>{props.helper}</P> : null}
        </BaseSelect>
    )

};

Select.propTypes = {
    required: PropTypes.bool,
    disabled: PropTypes.bool,
    title: PropTypes.string,
    items: PropTypes.array,
    error: PropTypes.string,
    helper: PropTypes.string,
};

export default withTheme(Select);
