import React from "react";
import {View, Text, TextInput, StyleSheet} from "react-native";
import P from "./P";
import styled, {withTheme} from "styled-components";
import PropTypes from "prop-types";
import {useSelector} from "react-redux";

const Styled = styled.TextInput`
    border-bottom-width: ${(props) => props.noBorder ? "0px" : StyleSheet.hairlineWidth};
    border-bottom-color: ${(props) => props.error ? props.theme.colors.error : props.theme.colors.textLight};
    color:  ${({theme}) => theme.colors.text};
    padding-top: ${({theme}) => theme.margins.side / 2}px;
    padding-bottom: ${({theme}) => theme.margins.side / 2}px;
    flex-direction: row;
    font-size: 18px;
    align-items: center;
`;

const Wrapper = styled.View`
    margin-right: ${({theme}) => theme.margins.side * 2}px;
    margin-left: ${({theme}) => theme.margins.side * 2}px;
    margin-top: ${({theme}) => theme.margins.side}px;
    margin-bottom: ${({theme}) => theme.margins.side}px;
`;

const Input = React.forwardRef((props, ref) => {
    const themeName = useSelector(({theme})=>theme);
    return (
        <Wrapper style={props.style}>
            {props.label && <P color={props.error ? "error" : "textLight"} style={styles.label}>{props.label}</P>}
            <Styled
                // enablesReturnKeyAutomatically
                keyboardAppearance={themeName}
                ref={ref}
                underlineColorAndroid="transparent"
                autoCapitalize="none"
                placeholderTextColor={props.theme.colors.textLight}
                autoCorrect={false}
                style={[props.style, !props.valid && props.touched ? styles.invalid : null]}
                {...props}
            />
            {props.helperText && <P color={props.error ? "error" : "textLight"}>{props.helperText}</P>}
        </Wrapper>
    )
});

const styles = StyleSheet.create({
    invalid: {
        backgroundColor: '#f9c0c0',
        borderColor: "red"
    },
    label: {
        textTransform: "capitalize"
    }
});

Input.propTypes = {
    disabled: PropTypes.bool,
    label: PropTypes.string,
    error: PropTypes.bool,
    placeholder: PropTypes.string,
    helperText: PropTypes.string
};


export default withTheme(Input);
