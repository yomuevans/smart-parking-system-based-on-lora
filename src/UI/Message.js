import React from "react";
import {View, Dimensions, Modal, TouchableOpacity} from "react-native";
import {withTheme} from "styled-components";
import styled from "styled-components";
import PropTypes from 'prop-types';
import {hideMessage} from "../store/actions/message.actions";
import P from "./P";
import IconButton from "./IconButton";
import {useDispatch, useSelector} from "react-redux";
import Card from "./Card";


// const Styled = styled.Modal`
//     background-color: ${props => props.theme.colors.bgLight};
//     margin-right: ${props => props.theme.margins.side};
//     margin-left: ${props => props.theme.margins.side};
//     margin-bottom: ${props => props.theme.margins.side*2};
//     flex: 1;
//     border-radius: ${props => props.theme.baseRadius};
//     overflow: hidden;
//     padding: ${props=> props.hasPadding ? props.theme.margins.side*2 : 0}px;
// `;

const Message = (props) => {
    const dispatch = useDispatch();
    const message = useSelector(({message}) => message);

    // console.log(message);

    const t = setTimeout(()=>{
        dispatch(hideMessage())
    }, message.options.autoHideDuration);

    return (
        <Modal
            onShow={()=>{
                clearTimeout(t);
                t;
            }}
            presentationStyle={"overFullScreen"}
            animationType="fade"
            transparent={true}
            visible={message.state}>
            <Card style={{
                position: "absolute",
                bottom: 10,
                right: 10,
                left: 10,
                backgroundColor: props.theme.colors[message.options.color]
            }}>
                <View style={{flexDirection: "row",paddingTop: 10, paddingBottom: 10,paddingLeft:15, paddingRight: 15,  alignItems: "center"}}>
                    <P style={{ flex: 1 }} type={"h6"}>{message.options.message}</P>
                    <IconButton name={"close"} onPress={()=>dispatch(hideMessage())} size={15} />
                </View>
            </Card>
        </Modal>
    )
};


export default withTheme(Message);

