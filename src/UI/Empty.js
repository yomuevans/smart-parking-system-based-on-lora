import React from "react";
import {
    View,
    ActivityIndicator,
} from "react-native";
import {useSelector} from "react-redux";
import styled from "styled-components";
import {withTheme} from "styled-components";
import Icon from "react-native-vector-icons/Ionicons";
import P from "./P";


const Empty = ({theme}) => {
    const loading = useSelector(({loading}) => loading);

    return (
        <View style={{marginTop: 40, marginBottom: 40, alignItems: "center"}}>
            {loading ? <ActivityIndicator size={50} color={theme.colors.textLight}/> :
                <>
                    <Icon name={"ios-sad"} size={40} color={theme.colors.textLight} style={{marginBottom: 20}}/>
                    <P type={"h5"} color={"textLight"} align={"center"}>Sorry, We don't have anything for you.</P>
                </>
            }
        </View>
    )
};


export default withTheme(Empty);
