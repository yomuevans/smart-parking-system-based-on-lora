import React from "react";
import {
    TouchableOpacity,
    TouchableNativeFeedback,
    Text,
    View,
    StyleSheet,
    Platform
} from "react-native";
import Icon from "react-native-vector-icons/Ionicons";
import {withTheme} from "styled-components";
import PropTypes from "prop-types";

const IconButton = props => {
    // const content = (
    //     <View
    //         style={[
    //             styles.button,
    //             { backgroundColor: props.color },
    //             props.disabled ? styles.disabled : null
    //         ]}
    //     >
    //         <Text style={props.disabled ? styles.disabledText : null}>
    //             {props.children}
    //         </Text>
    //     </View>
    // );
    // if (props.disabled) {
    //     return content;
    // }
    // if (Platform.OS === "android") {
    //     return (
    //         <TouchableNativeFeedback onPress={props.onPress} {...props}>
    //             {content}
    //         </TouchableNativeFeedback>
    //     );
    // }
    return <TouchableOpacity onPress={props.onPress} {...props} style={[{alignItems: "center", justifyContent: "center", width: props.size || 60, height: props.size || 60}, props.style]}>
            <Icon color={props.theme.colors.text || props.color} size={24} name={(Platform.OS === "android") ? "md-"+props.name : "ios-"+props.name} />
        </TouchableOpacity>;
};
//
// const styles = StyleSheet.create({
//     disabled: {
//         backgroundColor: "#eee",
//         borderColor: "#aaa"
//     },
//     disabledText: {
//         color: "#aaa"
//     }
// });

IconButton.propTypes = {
    color: PropTypes.oneOf([
        "primary",
        "primaryDark",
        "error",
        "warning",
        "success",
        "info",
        "errorDark",
        "warningDark",
        "successDark",
        "infoDark",
        "purple",
        "purpleDark",
        "orange",
        "orangeDark",
        "bg",
        "bgLight",
        "text",
        "textLight",
        "border",
    ]),
    name:  PropTypes.string,
    size: PropTypes.number
};

export default withTheme(IconButton);
