import React from "react";
import {StyleSheet, Text} from "react-native";
import {withTheme} from "styled-components";
import styled from "styled-components";
import PropTypes from 'prop-types';

const Styled = styled.Text`
  color: ${props => props.color ? props.theme.colors[props.color] : props.theme.colors.text};
  font-size: ${props => props.theme.typo[props.type || "p"]}
  text-align: ${props => props.align || "auto"};
  font-weight: ${props => props.type ? "bold" : "normal"}
`;

const P = (props) => {
    return (
         <Styled {...props}>{props.children}</Styled>
    )
};

P.propTypes = {
    color: PropTypes.oneOf([
        "primary",
        "primaryDark",
        "error",
        "warning",
        "success",
        "info",
        "errorDark",
        "warningDark",
        "successDark",
        "infoDark",
        "purple",
        "purpleDark",
        "orange",
        "orangeDark",
        "bg",
        "bgLight",
        "text",
        "textLight",
        "border",
    ]),
    type: PropTypes.oneOf(['h1', 'h2', 'h3', 'h4', 'h5', 'h6', "small"]),
    align: PropTypes.oneOf(['center', 'right', 'left']),
};

export default withTheme(P);

