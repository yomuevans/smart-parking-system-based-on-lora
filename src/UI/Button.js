import React from "react";
import {
    TouchableOpacity,
    TouchableNativeFeedback,
    Text,
    View,
    StyleSheet,
    Platform
} from "react-native";
import styled from "styled-components";
import Icon from "react-native-vector-icons/Ionicons";
import PropTypes from "prop-types";
import P from "./P";
import {withTheme} from "styled-components";

const ButtonInner = (props) => {
    if(!props.disabled) {
        // if (Platform.OS === "android") return (<TouchableNativeFeedback {...props}/>);
        return (<TouchableOpacity {...props}/>);
    }
    else return (<View {...props}/>)
};

const BaseButton = styled(ButtonInner)`
    padding: ${props => props.theme.margins.side}px;
    align-items: center;
    flex-direction: row;
    justify-content: center;
`;

const Button = props => {
    return (
        <BaseButton {...props}>
            {props.icon && <Icon style={{marginLeft: 10, marginRight:20}} color={props.color ? props.theme.colors[props.color] : props.theme.colors.text} size={26} name={(Platform.OS === "android") ? "ios-"+props.icon : "md-"+props.icon} />}
            <P align={"center"} type={"h5"} color={props.disabled ? "border" : props.color}>
                {props.children}
            </P>
        </BaseButton>
    )

};

Button.propTypes = {
    disabled:  PropTypes.bool,
    icon:      PropTypes.string,
    color:     PropTypes.oneOf(['bg', 'bgLight', 'text', 'textLight', 'primary', 'border', 'error', 'warning','success','info']),
};

export default withTheme(Button);
