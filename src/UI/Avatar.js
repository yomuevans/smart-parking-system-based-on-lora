import React from "react";
import {View, Image, Text} from "react-native";
import {withTheme} from "styled-components";
import styled from "styled-components";
import PropTypes from 'prop-types';

const AvatarInner = (props) => {
    const {resize, size, alt, showFull} = props;
    // source should be available
    if (props.source && props.source.uri && props.source.uri !== '' && props.source.uri !== "null") {
        const src = props.source.uri;
        const newSrc = ((resize) ? `${src}/resize/${size}x${size}` : (size) ? `${src}/crop/${size}x${size}` : src);
        return (<Image  {...props} source={{uri: newSrc}}/>);
    } else {
        const letters = (alt) ? alt[0] + (alt[1] ? alt[1] : "") : "";
        return (<View {...props}><Text style={{
            textAlign: "center",
            color: props.theme.colors.textLight,
            textTransform: "uppercase",
            fontSize: 18,
            fontWeight: "bold"
        }}>{letters}</Text></View>)
    }
};

const Styled = styled(AvatarInner)`
    background-color: ${props => props.theme.colors.bg};
    width: ${props => props.size || 40}px;
    height: ${props => props.size || 40}px;
    border-radius: 100px;
    align-items: center; 
    justify-content :center
`;


const Avatar = (props) => {
    return (
        <Styled {...props} />
    )
};

Avatar.propTypes = {
    alt: PropTypes.string,
    size: PropTypes.number
};

export default withTheme(Avatar);

