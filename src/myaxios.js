import React from 'react';
import axios from 'axios';
import AsyncStorage from "@react-native-community/async-storage";
import store from "./store/store";
import {finishLoading} from "./store/actions/loading.action";
import {showMessage} from "./store/actions/message.actions";

export const baseURL = 'http://192.168.0.112:8080/api/v.1';
export const headers = {
    'Access-Control-Allow-Origin': '*',
    'Content-Type': 'application/json',
    // 'Authorization': 'Bearer $2a$10$I49vzP56RBiMpgWsAVz8Wep1diqELvofSsHXu0q391oJc6qQUN8hC',
};

const instance = axios.create({
    baseURL: baseURL,
    headers: headers
});

AsyncStorage.getItem('sps:token').then(token => {
    instance.interceptors.request.use(function (config) {
        // config.headers.token = token;
        config.headers.authorization = 'Bearer ' + (store.getState().user.token || token);
        return config;
    }, function (error) {
        return Promise.reject(error);
    });
});


// // // Add a request interceptor
// instance.interceptors.request.use(function (config) {
//     config.headers.token = localStorage.getItem('token');
//
//     return config;
// }, function (error) {
//     return Promise.reject(error);
// });

instance.interceptors.response.use(
    function (response) {
        // if (!!response.data.message) {
        //     store.dispatch(showMessage({color: "success",message: error.response.data.message}));
        // }
        return response;
    }, function (error) {
        store.dispatch(finishLoading());
        if(error.response) {
            console.log(error.response)
            if (!!error.response.data.message) {
                store.dispatch(showMessage({color: "error",message: error.response.data.message}));
            }
             if( error.response.status === 400) {
             }
             if ( error.response.status === 401) {
             }
            if(error.response.status === 441) {
            } else {
            }
        }
    return Promise.reject(error);
});

export default instance;
