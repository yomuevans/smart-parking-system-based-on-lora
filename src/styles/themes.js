// import { createTheming } from '@callstack/react-theme-provider';

export const themeBase = {
    typo: {
        h1: 32,
        h2: 28,
        h3: 24,
        h4: 20,
        h5: 18,
        h6: 16,
        p: 14,
        caption: 12,
        small: 10,
    },
    margins: {
        side: 10
    },
    baseRadius: 20,
};

export const colors = {
    white: "#fff",
    primary: "#3FBFD9",
    primaryDark: "#3aa4bb",
    error: "#e17055",
    warning: "#ffeaa7",
    success: "#4aef80",
    info: "#74b9ff",
    errorDark: "#d63031",
    warningDark: "#fdcb6e",
    successDark: "#00b894",
    infoDark: "#0984e3",
    purple: "#a29bfe",
    purpleDark: "#6c5ce7",
    orange: "#fab1a0",
    orangeDark: "#e17055"
};

export const themes = {
    dark: {
        ...themeBase,
        colors: {
            ...colors,
            bg: '#000000',
            bgLight: "#252525",
            text: "white",
            textLight: "#a5a5a5",
            border: "#444444",
        },
        accentColor: "#458622",
        textColor: "#504f4d",
        secondaryColor: "#7F5315",
    },
    light: {
        ...themeBase,
        colors: {
            ...colors,
            bg: '#EDEBEB',
            bgLight: "#FFF",
            text: "#707070",
            textLight: "#A7A7A7",
            border: "#cccccc",
        },
        accentColor: "#458622",
        textColor: "#D94B2B",
        secondaryColor: "#B9667F",
    }
};
